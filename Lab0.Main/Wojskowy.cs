﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    enum Rodzaj { Nawodny, Podwodny }  

    class Wojskowy : Statek
    {
        private int LiczbaDział;
        private Rodzaj Typ;

        public void WystrzelPocisk()
        {
            Console.WriteLine("Wystrzelono pocisk!");
        }

        public override string WlaczSilnik()
        {
            string s = "Wojskowy okręt włączony!";
            return s;
        }
    }
}
