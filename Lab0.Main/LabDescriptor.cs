﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Statek);
        public static Type B = typeof(Pasażerski);
        public static Type C = typeof(Wojskowy);

        public static string commonMethodName = "WlaczSilnik";
    }
}
